javascript: (function () {
    var x, y, z, uspl;
    if (!document.location.href.match(/https?:\/\/www\.flickr\.com\/photos\/[^\/]+\/\d+.*/)) {
        alert("This is the wrong site for that call");
        return;
    }
    if (document.location.href.match(/https?:\/\/www\.flickr\.com\/photos\/[^\/]+\/\d+.*/)) {

        x = document.location.href;
        uspl = x.split("/");

        var n = document.getElementsByClassName("context-list")[0].getElementsByTagName("li").length;

        for (i = 0; i < n; i++) {

            newURL = document.getElementsByClassName("context-list")[0].getElementsByTagName("li")[i].children[0].href + "pool/" + uspl[4];

            var picURL = document.getElementsByClassName("context-list")[0].getElementsByTagName("li")[i].children[0].href;
            var txtURL = document.getElementsByClassName("context-list")[0].getElementsByTagName("li")[i].children[1].firstChild.href;
            if ((picURL.match(/pool/g) || txtURL.match(/pool/g))) {
                return;
            }
            document.getElementsByClassName("context-list")[0].getElementsByTagName("li")[i].children[0].href = newURL;
            document.getElementsByClassName("context-list")[0].getElementsByTagName("li")[i].children[0].target = "_blank";
            document.getElementsByClassName("context-list")[0].getElementsByTagName("li")[i].children[0].rel = "noopener noreferrer";
            document.getElementsByClassName("context-list")[0].getElementsByTagName("li")[i].children[1].firstChild.href = newURL;
            document.getElementsByClassName("context-list")[0].getElementsByTagName("li")[i].children[1].firstChild.target = "_blank";
            document.getElementsByClassName("context-list")[0].getElementsByTagName("li")[i].children[1].firstChild.rel = "noopener noreferrer";

        }
    }

    function sleep(milliseconds) {
        var start = new Date().getTime();
        for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds) {
                break;
            }
        }
    }

    function openInNewTab(url) {
        var win = window.open(url, '_blank');
    }

})();