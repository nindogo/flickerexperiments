// ==UserScript==
// @name         flickrGroupPublicOrNot
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  puts a frame around private/family/friend photos in groups on flickr
// @author       You
// @match        https://www.flickr.com/*
// @include      *
// @grant        none
// @require      https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js
// @require         https://gist.githubusercontent.com/raw/2625891/waitForKeyElements.js
// ==/UserScript==

waitForKeyElements(".view .photo-list-photo-view", seeString);
/* waitForKeyElements(".photo-list-photo-interaction", seeString, 1); */


function seeString(jNode){
    if (document.location.href.match(/https?:\/\/www\.flickr\.com\/groups\/[^\/]+\/pool\//)){
        var re = /url\("?\/\/.+\/(.+?)_/
        var x = re.exec(jNode[0].attributes.style.nodeValue)[1];
        var url = "https://api.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key=d7ad093bc750dd85ae354f47fc9748d0&format=json&nojsoncallback=1&photo_id=" + x
        var request = new XMLHttpRequest();
        request.open("GET", url, false);
        request.send();
        var y = JSON.parse(request.responseText);
        if (y.stat == "fail") {
        jNode[0].style.border = '10px dotted red';}

    }
}