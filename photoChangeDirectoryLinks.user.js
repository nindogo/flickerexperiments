// ==UserScript==
// @name            flickrPhotoChangeGroupLinks
// @namespace       http://tampermonkey.net/
// @version         20200126
// @description     Uses
// @require         https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js
//                  Selectors and the following script require jQuery.
// @require         https://gist.githubusercontent.com/raw/2625891/waitForKeyElements.js
//                  The previous require is from a script of Brock Adams (Thanks to him!)
// @description     On Flicker, the picture view shows all groups that this photo is in.
// @description     This userscript changes the destination of the links to show only the photos in those group
// @description     that are owned by the current photo's owner.
// @author          nindogo - following the ideas from {xxxx}
// @grant           none
// @include         http*://*flickr.com/people/*/groups/
// @include         http*://*flickr.com/photos/*
// @include         http*://www.flickr.com/groups/*/members/*
// @exclude         http*://www.flickr.com/photos/*/sets/*
// @downloadURL     https://bitbucket.org/nindogo/flickerexperiments/raw/master/photoChangeDirectoryLinks.user.js
// ==/UserScript==

waitForKeyElements(".sub-photo-groups-view a", modifyLinksOnPhotos);
waitForKeyElements(".show-after-locked a", modifyLinksOnUserGroupsPage);
waitForKeyElements(".groups-members a", modifyLinksOnGroupMembersPage);


function modifyLinksOnPhotos(jNode) {
    if (document.location.href.match(/https?:\/\/(?:www.|)flickr\.com\/photos\/[^\/]+\/\d+.*/)) {
        var x = document.location.href;
        var uspl = x.split("/");
        jNode[0].href = jNode[0].href + "pool/" + uspl[4];
        jNode[0].target = "_blank";
        jNode[0].rel = "noopener noreferrer"
        console.log(jNode[0].href);
        console.log(jNode[0].target);
        console.log(jNode[0].rel);
    }
}


function modifyLinksOnUserGroupsPage(jNode) {
    if (document.location.href.match(/https?:\/\/(?:www.|)flickr\.com\/people\/[^\/]+\/groups\//)) {
        var x = document.location.href;
        var uspl = x.split("/");
        jNode[0].href = jNode[0].href + "pool/" + uspl[4];
        jNode[0].target = "_blank";
        jNode[0].rel = "noopener noreferrer"
        console.log(jNode[0].target);
        console.log(jNode[0].rel);
        console.log(jNode[0].href)
    }
}


function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


function modifyLinksOnGroupMembersPage(jNode) {
    console.log("Tumeanza");
    if (document.location.href.match(/https?:\/\/(?:www.|)flickr\.com\/groups\/[^\/]+\/members\//)) {
        var i;
        var n = $(".groups-members a").length;
        //i = n - 1;
        var x = document.location.href;
        var uspl = x.split("/");
        for (i = (n - 1); i > -1; i--) {
            var elementURL = $(".groups-members a")[i].href;
            if (elementURL.match(/pool/g)) {
                break;
            }
            var g = elementURL.split("/");
            var newURL = "https://www.flickr.com/groups/" + uspl[4] + "/pool/" + g[4];
            $(".groups-members a")[i].href = newURL;
            $(".groups-members a")[i].target = "_blank";
            $(".groups-members a")[i].rel = "noopener noreferrer";
        }
    }

    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }



}