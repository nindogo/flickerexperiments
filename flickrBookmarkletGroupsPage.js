javascript:(function () {
    var x, y, z, uspl;
    if (!document.location.href.match(/https?:\/\/www\.flickr\.com\/people\/[^\/]+\/groups\//)) {
        alert("This is the wrong site for that call");
        return;
    }
    if (document.location.href.match(/https?:\/\/www\.flickr\.com\/people\/[^\/]+\/groups\//)) {
        
        x = document.location.href;
        uspl = x.split("/");

        var n = document.getElementsByClassName("show-after-locked").length;

        for (i = 0; i < n; i++) {

            newURL = document.getElementsByClassName("show-after-locked")[i].getElementsByTagName("a")[0].href + "pool/" + uspl[4];
            txtURL = document.getElementsByClassName("show-after-locked")[i].getElementsByTagName("a")[0].href;

            if (txtURL.match(/pool/g)) {
                return;
            }
            document.getElementsByClassName("show-after-locked")[i].getElementsByTagName("a")[0].href = newURL;
            document.getElementsByClassName("show-after-locked")[i].getElementsByTagName("a")[0].target = "_blank";
            document.getElementsByClassName("show-after-locked")[i].getElementsByTagName("a")[0].rel = "noopener noreferrer";


        }
    }

})();